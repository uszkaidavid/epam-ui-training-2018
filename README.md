# Simple Ecommerce App
### Web application for \<epam> UI trainings in the Debrecen office.

#### Prerequisites
* [NodeJS (8.6.0)](https://nodejs.org/en/)
* NPM
* [Gulp](https://gulpjs.com/)
* [Yarn](https://yarnpkg.com/en/)

#### Extend the PATH environment variable

Please make sure that the PATH environment variable contains the following (please adjust them if you have different installation paths):

```
C:\Program Files (x86)\Yarn\bin\
C:\Program files\nodejs
%APPDATA%\npm
C:\Windows\System32
C:\Program Files\Git\cmd
```

#### Install Gulp

```
npm install -g gulp
```


#### Install Dependencies
```
yarn install
```

or

```
npm install
```

#### Build
```
gulp build
```

### Run
```
gulp
```
